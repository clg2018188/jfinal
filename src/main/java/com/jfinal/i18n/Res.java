/**
 * Copyright (c) 2011-2023, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jfinal.i18n;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import com.jfinal.config.ConfigPara;
import com.jfinal.kit.StrKit;

/**
 * Res is used to get the message value from the ResourceBundle of the related Locale.
 */
public class Res {
	
	private final ResourceBundle resourceBundle;
	
	public Res(String baseName, String locale) {
		if (StrKit.isBlank(baseName)) {
			throw new IllegalArgumentException("baseName can not be blank");
		}
		if (StrKit.isBlank(locale)) {
			throw new IllegalArgumentException("locale can not be blank, the format like this: zh_CN or en_US");
		}
		
		this.resourceBundle = ResourceBundle.getBundle(baseName, I18n.toLocale(locale));
	}
	
	/**
	 * Get the message value from ResourceBundle of the related Locale.
	 * @param key message key
	 * @return message value
	 */
	public String get(String key) {
		if (!resourceBundle.containsKey(key)) {
			return key;
		}
		String value = resourceBundle.getString(key);
		if (value.indexOf("${") >= 0) {
			// ${}进行替换处理
			value = parseValue(value);
		}
		return value;
	}
	
	/**
	 * Get the message value from ResourceBundle by the key then format with the arguments.
	 * Example:<br>
	 * In resource file : msg=Hello {0}, today is{1}.<br>
	 * In java code : res.format("msg", "james", new Date()); <br>
	 * In freemarker template : ${_res.format("msg", "james", new Date())}<br>
	 * The result is : Hello james, today is 2015-04-14.
	 */
	public String format(String key, Object... arguments) {
		if (!resourceBundle.containsKey(key)) {
			return key;
		}
		String value = resourceBundle.getString(key);
		if (value.indexOf("${") >= 0) {
			// ${}进行替换处理
			value = parseValue(value);
		}
		return MessageFormat.format(value, arguments);
	}
	
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

    /**
     * 格式化属性值里面的${}格式内部引用
     * @param value
     * @return
     */
    private String parseValue(String value) {
        List<ConfigPara> paras = parseParas(value);
        if (paras == null || paras.size() == 0) {
            return value;
        }
        StringBuilder retBuilder = new StringBuilder(value.length());
        int index = 0;
        for (ConfigPara para : paras) {
            if (para.getStart() > index) {
                retBuilder.append(value, index, para.getStart());
            }

            String configValue = resourceBundle.getString(para.getKey());
            configValue = isNotBlank(configValue) ? configValue : para.getDefaultValue();
            retBuilder.append(configValue);
            index = para.getEnd() + 1;
        }

        if (index < value.length()) {
            retBuilder.append(value, index, value.length());
        }

        return retBuilder.toString();
    }

    /**
     * 匹配属性值里面的${}格式内部引用
     * @param string
     * @return
     */
    public static List<ConfigPara> parseParas(String string) {
        if (isBlank(string)) {
            return null;
        }

        List<ConfigPara> paras = null;
        ConfigPara para = null;
        int index = 0;
        boolean hasDefaultValue = false;
        char[] chars = string.toCharArray();
        for (char c : chars) {
            //第一个字符是 '{' 会出现 ArrayIndexOutOfBoundsException 错误
            if (c == '{' && index > 0 && chars[index - 1] == '$' && para == null) {
                para = new ConfigPara();
                hasDefaultValue = false;
                para.setStart(index - 1);
            } else if (c == '}' && para != null) {
                para.setEnd(index);
                if (paras == null) {
                    paras = new LinkedList<>();
                }
                paras.add(para);
                para = null;
            } else if (para != null) {
                if (c == ':' && !hasDefaultValue) {
                    hasDefaultValue = true;
                } else if (hasDefaultValue) {
                    para.appendToDefaultValue(c);
                } else {
                    para.appendToKey(c);
                }
            }
            index++;
        }
        return paras;
    }

    public static boolean isBlank(String str) {
        if (str == null) {
            return true;
        }

        for (int i = 0, len = str.length(); i < len; i++) {
            if (str.charAt(i) > ' ') {
                return false;
            }
        }
        return true;
    }

    public static boolean isNotBlank(Object str) {
        return str != null && !isBlank(str.toString());
    }
}
